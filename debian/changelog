python-django-gravatar2 (1.4.5-1) unstable; urgency=medium

  * Team Upload
  * Fix d/watch for new PyPi naming policy
  * Use dh-sequence-python3
  * New upstream version 1.4.5

 -- Alexandre Detiste <tchet@debian.org>  Fri, 20 Dec 2024 23:41:06 +0100

python-django-gravatar2 (1.4.4-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 18:51:33 +0000

python-django-gravatar2 (1.4.4-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 00:43:06 -0400

python-django-gravatar2 (1.4.4-2) unstable; urgency=medium

  * Run wrap-and-sort.
  * Add myself to Uploaders.
  * Allow stderr in autopkgtests.

 -- Michael Hudson-Doyle <mwhudson@debian.org>  Tue, 09 Jun 2020 14:29:33 +1200

python-django-gravatar2 (1.4.4-1) unstable; urgency=medium

  * New upstream release 1.4.4
  * Use my Debian address
  * Drop py2 tests (Closes: #941012, #943220)
  * Bump Standards-Version to 4.5.0
  * Set Rules-Requires-Root to no
  * Bump debhelper-compat to 12
  * Sets up proper autopkgtests
  * Add ca-certificates as a Recommend.
    It is not required to have a working package, but SSL won't work without
    it.
  * Add a comment at d/rules to explain why the tests are not run at
    buildtime.

 -- Pierre-Elliott Bécue <peb@debian.org>  Wed, 15 Apr 2020 18:00:45 +0200

python-django-gravatar2 (1.4.2-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jul 2019 23:14:44 +0200

python-django-gravatar2 (1.4.2-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP

  [ Pierre-Elliott Bécue ]
  * d/control:
    - Bump Standards-Version to 4.2.0. No change required.
    - Lower the Build-Depends version for debhelper from 11 to 11~
  * d/tests/control:
    - Add python3-all to the autopkgtest for python3 package. (Closes:
      #904296)

 -- Pierre-Elliott Bécue <becue@crans.org>  Mon, 13 Aug 2018 22:28:56 +0200

python-django-gravatar2 (1.4.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Pierre-Elliott Bécue ]
  * d/control:
    - Swap Uploaders/Maintainer field.
    - Set dh dependency version to 11
    - Update dependeny fields
    - Remove Testsuite header as d/t/control exists now
    - Bump Standards-Version to 4.1.3
  * d/compat to 11
  * d/watch to version 4 + uscan tags
  * d/tests/control: add basic tests that will work
  * Drop git-dpm

 -- Pierre-Elliott Bécue <becue@crans.org>  Sat, 24 Feb 2018 15:56:42 +0100

python-django-gravatar2 (1.4.2-1) unstable; urgency=medium

  * New upstream release
   - Django 1.10 and 1.11 support
   - Updates RELEASING doc
   - License specification in setup.py
  * Updates standards to 4.0.0
  * Adds testsuite
  * Migrates the VCS on git.debian.org/python-modules

 -- Pierre-Elliott Bécue <becue@crans.org>  Mon, 31 Jul 2017 12:49:16 +0200

python-django-gravatar2 (1.4.0-1) unstable; urgency=low

  * Initial release (Closes: #819183)

 -- Pierre-Elliott Bécue <becue@crans.org>  Wed, 23 Mar 2016 22:37:40 -0400
